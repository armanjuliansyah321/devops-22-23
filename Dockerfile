FROM nginx:latest

RUN apt-get update -y && apt-get upgrade -y

WORKDIR /usr/share/nginx/html

COPY ./softland/ /usr/share/nginx/html
